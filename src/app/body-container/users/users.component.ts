import {Component, ElementRef, OnInit, Input} from '@angular/core';
import {PostListService} from '../service/post-list.service';
import { Observable } from 'rxjs';
import { UsersI } from '../service/users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [PostListService]
})

export class UsersComponent implements OnInit {
  constructor( private dataWp: PostListService) { }

  @Input() post: UsersI;

  post$: Observable<UsersI[]>;

  ngOnInit() {
    this.dataWp.getPosts('/users').subscribe(
      (data) => {
        this.post = data[''];
        console.log(data);
      }
    );
    this.post$ = this.dataWp.getPosts('/users');
    console.log(this.post$);
  }
}
