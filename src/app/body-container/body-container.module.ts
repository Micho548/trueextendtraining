import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import {FlexModule} from '@angular/flex-layout';
import {MatButtonModule, MatCardModule, MatChipsModule, MatIconModule} from '@angular/material';



@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    FlexModule,
    MatCardModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule
  ]
})
export class BodyContainerModule { }
