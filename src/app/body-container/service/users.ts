export interface UsersI {
  avatar_url: string;
  login: string;
  url: string;
  repos_url: string;
}
