import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UsersI} from './users';

@Injectable()

export class PostListService {

  constructor( private http: HttpClient ) { }
  urlApi =  'https://api.github.com';

  repo( repo: string) {
    return this.urlApi + repo;
  }

  getPosts(endPoint: string): Observable<UsersI[]> {
    return this.http.get<UsersI[]>(this.urlApi + endPoint, {
      params: { per_page: '6' }
    });
  }
}
